import kertolasku from "./util/kertolasku.js";

/**
 * Tämä on kohtuullisen turha funktio jonka tarkoitus on vain viedä tilaa
 * tekstieditorissa.
 * 
 * @param {*} ihanTotta omituinen ja löyhä parametri
 * @returns palauttaa aina oikean
 */

function täysinTurhaSwitchCase(ihanTotta) {
  switch (ihanTotta) {
    case 1:
      break;
    case 2:
      break;
    case 3:
      break;
    case 4: 
      break;
    case "Miksi?":
      let hevonen = "Matti";
      let koira = "Teppo";
      let hevosKoira = hevonen + koira;
      console.log(hevosKoira);
      break;
    case true:
      // En valehtele
      break;
    case false:
      // Syötän pajunköyttä
      break;
    default:
      break;
  }
  return 42;
}

console.log(kertolasku(10, 5));
